Original Project by [Liv](https://github.com/l1ving/)

# holybible.guru

I made this as a joke so I could redirect people to the Arch Wiki

https://holybible.guru

## Update:

The domain expires 2021-07-26 and it costs much more than it's worth as a joke domain to renew (~$56 CAD). As such, I'm archiving this repository and letting the domain expire when it does.

## Update II (Aven):
I bought the Domain for like $3.50 USD, so here we are
